window.onscroll = function() {
    let scrolled = window.pageYOffset;

    let elem = document.getElementById('aboutSection');
    let elem2 = document.getElementById('parallax2Section');
    let elem3 = document.getElementById('teamSection');
    let elem4 = document.getElementById('parallax3Section');
    let elem5 = document.getElementById('workSection');


    function scrollToBottom (element, triggerPoint) {
        if (scrolled >= triggerPoint) {
            return element.classList.add('visible');
        } else {
            return element.classList.remove('visible');
        }
    }
    scrollToBottom(elem, 400);
    scrollToBottom(elem2, 650);
    scrollToBottom(elem3, 1400);
    scrollToBottom(elem4, 2100);
    scrollToBottom(elem5, 2500);
}


